# Summary 
[Espresso Machine Requirements](em-requirements.md)
- [Introduction](introduction.md)
- [Overall Description](overall-description.md)
- [Requirements](requirements.md)
    - [Functional Requirements](requirements/funcional.md)
    - [Nonfunctional Requirements](requirements/nonfunctional.md)

 [Changelog](changelog.md)
